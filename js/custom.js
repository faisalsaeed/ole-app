// Custom JS 

/*
* @author: Faisal Saeed
*/

/*app carousel*/
$(document).ready(function () {

  /*app secreens carousel*/
  var swiper = new Swiper(".app-screens", {
    slidesPerView: 1,
    effect: "fade",
    grabCursor: true,
    autoplay: {
      delay: 4000,
      disableOnInteraction: false,
    },
    pagination: {
      el: ".swiper-pagination",
      dynamicBullets: true,
    },
  });

  /*growing with us*/
  var swiper = new Swiper(".growing-with-us", {
    slidesPerView: 1,
    spaceBetween: 10,
    grabCursor: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
    },
  });

  /*landing items*/
  var swiper = new Swiper(".landing-items", {
    slidesPerView: 1,
    spaceBetween: 10,
    grabCursor: true,
    centeredSlides: true,
    loop: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 10,
      },
    },
  });

  /* app screenshoots*/
  var swiper = new Swiper(".app-screenshoots-slider", {
    slidesPerView: 1,
    spaceBetween: 50,
    grabCursor: true,
    centeredSlides: true,
    loop: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView:3,
        spaceBetween: 10,
      },
    },
  });


    /* app screenshoots*/
    var swiper = new Swiper(".line-up-app-slider", {
      slidesPerView: 1,
      spaceBetween: 20,
      grabCursor: true,
      centeredSlides: true,
      loop: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 5,
        },
        1024: {
          slidesPerView:5,
          spaceBetween: 10,
        },
      },
    });


  /*add class in header*/
  $(window).on("scroll", function () {
    if ($(window).scrollTop() >= 50) {
      $("header").addClass("header-scroll ");
    } else {
      $("header").removeClass("header-scroll ");
    }
  });



  /*scroll to top*/
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  });
  $('.scrollup').click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, 600);
    return false;
  });
});



